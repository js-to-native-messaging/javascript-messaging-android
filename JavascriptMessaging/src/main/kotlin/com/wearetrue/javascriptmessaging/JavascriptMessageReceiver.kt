package com.wearetrue.javascriptmessaging
/**
 * True Partners, LLC licenses this file to you under the MIT license.
 */
interface JavascriptMessageReceiver {
    fun didReceiveMessage(json:String)
}