package com.wearetrue.javascriptmessaging

import android.webkit.ValueCallback
import org.json.JSONException
import org.json.JSONObject
/**
 * True Partners, LLC licenses this file to you under the MIT license.
 */
typealias JavascriptMessageHandler = (message:JavascriptMessage) -> Unit

/**
 * Copyright (c) 2019 True Partners, LLC. All rights reserved.
 */
class JavascriptMessage : JSONObject {
    private var _callbackHandler: ((JavascriptMessage) -> Unit)? = null
    private val _propertyMap:MessagePropertyMap

    constructor() : super() {
        _propertyMap = MessagePropertyMap()
    }

    constructor(propertyMap:MessagePropertyMap = MessagePropertyMap()) : super() {
        _propertyMap = propertyMap
    }

    constructor(json: String, propertyMap:MessagePropertyMap = MessagePropertyMap()) : super(json) {
        _propertyMap = propertyMap
    }

    var callback: JavascriptMessageHandler?
        get() = _callbackHandler
        set(callback) {
            _callbackHandler = callback
        }

    var name: String
        get() = optString(_propertyMap.name)
        set(name) {
            setProperty(_propertyMap.name, name)
        }

    var id: String?
        get() = optString(_propertyMap.id)
        set(id) {
            setProperty(_propertyMap.id, id)
        }

    var responseTo: String?
        get() = optString(_propertyMap.responseTo)
        set(responseTo) {
            setProperty(_propertyMap.responseTo, responseTo)
        }

    var rsvp: Boolean
        get() = optBoolean(_propertyMap.rsvp, false)
        set(rsvp) {
            setProperty(_propertyMap.rsvp, rsvp)
        }

    var payload: JSONObject?
        get() = optJSONObject(_propertyMap.payload)
        set(payload) {
            if (payload != null) {
                setProperty(_propertyMap.payload, payload)
            }
            else {
                remove(_propertyMap.payload)
            }
        }

    fun createResponse(payload: JSONObject? = null) : JavascriptMessage {
        val response = JavascriptMessage(_propertyMap);

        response.name = name
        response.responseTo = id
        response.payload = payload

        return response;
    }

    private fun setProperty(name: String, value: Any?) {
        try {
            put(name, value)
        } catch (err: JSONException) {
            // swallow?
        }
    }
}