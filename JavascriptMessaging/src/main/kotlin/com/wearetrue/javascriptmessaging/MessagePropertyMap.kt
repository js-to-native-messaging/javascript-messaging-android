package com.wearetrue.javascriptmessaging
/**
 * True Partners, LLC licenses this file to you under the MIT license.
 */
public class MessagePropertyMap {
    private var _id = "id"
    private var _name = "name"
    private var _payload = "payload"
    private var _rsvp = "rsvp"
    private var _responseTo = "responseTo"

    var id: String
        get() = _id
        set(id) {
            _id = id
        }
    var name: String
        get() = _name
        set(name) {
            _name = name
        }
    var payload: String
        get() = _payload
        set(payload) {
            _payload = payload
        }
    var rsvp: String
        get() = _rsvp
        set(rsvp) {
            _rsvp = rsvp
        }
    var responseTo: String
        get() = _responseTo
        set(responseTo) {
            _responseTo = responseTo
        }
}