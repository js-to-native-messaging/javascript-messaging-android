package com.wearetrue.javascriptmessaging
import android.content.Context
import android.os.Handler
import android.webkit.JavascriptInterface
import android.webkit.WebView
/**
 * True Partners, LLC licenses this file to you under the MIT license.
 */
private const val SCRIPT_TEMPLATE = "%s_processWebviewMessage(%s);"

class WebviewMessageInterface constructor(val context: Context, val webView: WebView, val interfaceName: String = "nativeBridge", val scriptProcessorName:String = "nativeBridge", var receiver: JavascriptMessageReceiver? = null) : JavascriptMessageDispatcher {
    init {
        webView.addJavascriptInterface(this, interfaceName)
    }
    override fun dispatchMessage(json: String) {
        val script = String.format(SCRIPT_TEMPLATE, scriptProcessorName, json)
        val mainHandler = Handler(context.mainLooper)
        val invoker = Runnable { webView.evaluateJavascript(script, null) }
        mainHandler.post(invoker)
    }

    @JavascriptInterface
    fun postMessage(message: String?) {
        if (message != null) {
            receiver?.didReceiveMessage(message)
        }
    }
}