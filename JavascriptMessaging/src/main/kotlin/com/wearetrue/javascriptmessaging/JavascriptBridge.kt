package com.wearetrue.javascriptmessaging

import android.util.Log
import android.webkit.ValueCallback
import org.json.JSONObject
import java.util.*

/**
 * True Partners, LLC licenses this file to you under the MIT license.
 */

private const val LOG_TAG = "JavascriptBridge"

class JavascriptBridge constructor(val webviewMessageInterface: WebviewMessageInterface, initMessageName: String? = null, initMessageProcessor: ((message: JavascriptMessage) -> JSONObject?)? = null, private val propertyMap:MessagePropertyMap = MessagePropertyMap()) : JavascriptMessageReceiver {
    private val _initMessageName:String = initMessageName ?: String.format("%s.init", webviewMessageInterface.interfaceName)
    var initialized = false
        private set
    private var messageCount = 0
    private val _messageHandlers = HashMap<String, ValueCallback<JavascriptMessage>>()
    private val _callbackHandlers = HashMap<String, JavascriptMessageHandler>()
    private var _pendingMessages:MutableList<JavascriptMessage>? = mutableListOf<JavascriptMessage>()

    init {
        webviewMessageInterface.receiver = this
        _messageHandlers[_initMessageName] =  ValueCallback {
            initialized = true

            if (it.callback != null) {
                val payload:JSONObject? = initMessageProcessor?.invoke(it)
                val initResponse = it.createResponse(payload)

                try {
                    // be sure the init message is the first message sent
                    it.callback?.invoke(initResponse)

                    flushPending()
                } catch (err: Exception) {
                    // TODO: do something here
                }
            }
        }
    }

    fun setHandler(messageName: String, handler: ValueCallback<JavascriptMessage>) {
        if(messageName != _initMessageName) {
            _messageHandlers[messageName] = handler
        }
    }

    fun removeHandler(messageName: String) {
        if(messageName != _initMessageName && _messageHandlers.containsKey(messageName)) {
            _messageHandlers.remove(messageName);
        }
    }

    fun createMessage(name: String, payload: JSONObject? = null, callback: JavascriptMessageHandler? = null) : JavascriptMessage {
        val message = JavascriptMessage(propertyMap)

        message.name = name
        message.payload = payload

        if(callback != null) {
            message.rsvp = true
            message.callback = callback
        }

        return message
    }

    override fun didReceiveMessage(json: String) {
        try {
            val message = JavascriptMessage(json, propertyMap)
            if (message.responseTo != null && _callbackHandlers.containsKey(message.responseTo)) {
                val handler = _callbackHandlers[message.responseTo]
                handler!!.invoke(message)
                _callbackHandlers.remove(message.responseTo)
            } else if (_messageHandlers.containsKey(message.name)) {
                val handler = _messageHandlers[message.name]
                if (message.rsvp && message.id != null) {
                    message.callback =  {
                        it.responseTo = message.id
                        sendMessage(it)
                    }
                }
                handler!!.onReceiveValue(message)
            }
        } catch (err: Exception) {
            Log.e(LOG_TAG, "JavascriptBridge didReceiveMessage failed.", err)
        }
    }

    fun sendMessage(name: String, payload: JSONObject? = null, callback: JavascriptMessageHandler? = null) {
        val message = createMessage(name, payload, callback)

        sendMessage(message)
    }

    fun sendMessage(message: JavascriptMessage) {
        if(!initialized) {
            queueMessage(message)
        }
        else {
            dispatchMessage(message)
        }
    }

    private fun dispatchMessage(message: JavascriptMessage) {
        val callback = message.callback

        messageCount += 1;
        message.id = "${webviewMessageInterface.interfaceName})_$messageCount"

        if (callback != null) {
            _callbackHandlers[message.id!!] = callback;
        }

        webviewMessageInterface.dispatchMessage(message.toString())
    }

    private fun queueMessage(message: JavascriptMessage) {
        _pendingMessages?.add(message)
    }

    private fun flushPending() {
        if (_pendingMessages != null && initialized) {
            for (message in _pendingMessages!!) {
                dispatchMessage(message)
            }
        }

        _pendingMessages = null
    }
}